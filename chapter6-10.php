<?php

$startTime = "09:30";
$endTime = "18:45";

$workTime = totaltime($endTime) - totaltime($startTime);

$workHour = floor($workTime / 60);
$workMin = floor($workTime % 60);

echo strnum($workHour).":".strnum($workMin)."\n";


function totaltime($time){
  $arraytime = explode(":",$time);
  $hour = intval($arraytime[0]);
  $min = intval($arraytime[1]);

  $total = ($hour * 60) + $min;
  
  return $total;
}

function strnum($num) {
  if ($num <= 9){
    $num = "0".strval($num);
  }
  return $num;
}