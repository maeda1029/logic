<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="chapter5-12.css">
  </head>
  <body>
    <header>
      <div class="title">Adseed Books</div>
      <div class="menu">
        <p>トップ</p>
        <p>本一覧</p>
        <p>マイページ</p>
      </div>
    </header>
    <div class="container">
      <div class="content">
        <article class="wraptopimage">
          <img class="topimage" src="images/top_book.jpg">
        </article>
      </div>
      <article class="wrapboxs">
        <div class="box">
          <p>本を読んで知識を広げよう。日常生活のヒントがもらえるかもしれません。</p>
        </div>
        <div class="box"><img src="images/book_with_glass.jpg"></div>
        <div class="box"><img src="images/reading.jpg"></div>
        <div class="box">
          <p>読書にはストレス解消解消効果があります。</p>
        </div>
      </article>
    </div>
    <div class="wrapbookandform">
      <article class="wrapbooks">
        <h2>Book Review</h2>
        <table>
          <thead>
            <tr>
              <th>タイトル</th>
              <th>著者</th>
              <th>出版年</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>吾輩は猫である</td>
              <td>夏目漱石</td>
              <td>1911</td>
            </tr>
            <tr>
              <td>こころ</td>
              <td>夏目漱石</td>
              <td>1927</td>
            </tr>
            <tr>
              <td>坊ちゃん</td>
              <td>夏目漱石</td>
              <td>1950</td>
            </tr>
          </tbody>
        </table>
      </article>
      <form action="#" method="POST">
      <h2>投稿フォーム</h2>
				<table>
					<tbody>
						<tr>
							<th>タイトル：</th>
							<td><input type="text" name="title" size="30"></td>
						</tr>
						<tr>
							<th>著者：</th>
							<td><input type="text" name="author" size="30"></td>
						</tr>
						<tr>
							<th>ジャンル</th>
							<td>
								<input type="checkbox" name="genre" value="novel">小説・文学
								<input type="checkbox" name="genre" value="Non-Fiction">ノンフィクション
								<input type="checkbox" name="genre" value="mystery">ミステリー
								<input type="checkbox" name="genre" value="history">歴史
								<input type="checkbox" name="genre" value="business">ビジネス
							</td>
						</tr>
						<tr>
							<th>オススメ度：</th>
							<td>
								<select name="recommend">
									<option value="1">★</option>
									<option value="2">★★</option>
									<option value="3">★★★</option>
									<option value="4">★★★★</option>
									<option value="5">★★★★★</option>
								</select>
							</td>
						</tr>
						<tr>
							<th>匿名投稿：</th>
							<td>
								<input type="radio" name="postHide" value="1" checked="checked">匿名で投稿
								<input type="radio" name="postHide" value="0">ニックネームで投稿
							</td>
						</tr>
						<tr>
							<th colspan="2">
								<input type="submit" value="送信">
							</th>
						</tr>
					</tbody>
				</table>
      </form>
    </div>


  </body>

</html>    