<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="chapter5-10.css">
    <title>5-9</title>
  </head>
  <body>
    <header><h1>Adseed Books</h1><header>
    <div class="container">
        <div class="content">
          <article>
            <h2>新着レビュー</h2>
            <p>昨日のレビュー数：<?php echo $preWeekCount = 90;?>件</p>
            <p>今日のレビュー数：<?php echo $thisWeekCount = 100;?>件</p>
            <h2>Book Review</h2>
            <?php $books = [
              ["id" => 1, "title" => "吾輩は猫である", "author" => "夏目漱石", "yearPublished" => "1911"],
              ["id" => 2, "title" => "こころ", "author" => "夏目漱石", "yearPublished" => "1927"],
              ["id" => 3, "title" => "坊ちゃん", "author" => "夏目漱石", "yearPublished" => "1950"],
            ];?>
            <table>
              <thead>
                <tr>
                  <th>タイトル</th>
                  <th>著者</th>
                  <th>出版年</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach($books AS $book) { ?>
                <tr>
                  <td><?php echo $book["title"]; ?></td>
                  <td><?php echo $book["author"]; ?></td>
                  <td><?php echo $book["yearPublished"]; ?></td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </article>
          
        </div>
    </div>
    <footer></footer>
  </body>
</html>