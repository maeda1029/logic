<?php

$dateTime = "2020-03-05";

$year = substr($dateTime,0,4);
$month = substr($dateTime,5,6);
$day = substr($dateTime,8,9);

function Zero($value){
  if (substr($value,0,1) ==="0"){
    $value = substr($value,1,1);
  } 
  return $value;
}

echo Zero($year)."年".Zero($month)."月".Zero($day)."日\n";