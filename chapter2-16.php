<?php

$num1 = 0;
$num2 = 1;

for($i = 1; $i <= 100; $i++){
  if ($i === 1){
    echo "0\n";
  } elseif ($i === 2) {
    echo "1\n";
  } else {
    $num3 = $num2;
    $num2 = $num1 + $num2;
    echo $num2."\n";
    $num1 = $num3;
  }
}